/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.animal1;

/**
 *
 * @author OS
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println(h1);

        Crocodile cd1 = new Crocodile("Mitang");
        cd1.eat();
        cd1.crawl();
        cd1.walk();
        cd1.speak();
        cd1.sleep();
        System.out.println(cd1);

        Snake sn1 = new Snake("Aungpao");
        sn1.eat();
        sn1.crawl();
        sn1.walk();
        sn1.speak();
        sn1.sleep();
        System.out.println(sn1);

        Cat c1 = new Cat("Meaow");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println(c1);

        Dog d1 = new Dog("Lucky");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println(d1);

        Fish f1 = new Fish("BungBung");
        f1.eat();
        f1.swim();
        f1.walk();
        f1.speak();
        f1.sleep();
        System.out.println(f1);

        Crab cb1 = new Crab("Boonme");
        cb1.eat();
        cb1.swim();
        cb1.walk();
        cb1.speak();
        cb1.sleep();
        System.out.println(cb1);

        Bat b1 = new Bat("Lin");
        b1.eat();
        b1.fly();
        b1.walk();
        b1.speak();
        b1.sleep();
        System.out.println(b1);

        Bird b = new Bird("Michokh");
        b.eat();
        b.fly();
        b.walk();
        b.speak();
        b.sleep();
        System.out.println(b1);

        System.out.println("Polymorphism Test: ");
        Animal1[] animals = {h1, c1, cd1, sn1, b1, f1, d1, cb1, b};
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Animal1) {
                animals[i].eat();
                animals[i].walk();
                animals[i].speak();
                animals[i].sleep();

            }
            if (animals[i] instanceof LandAnimal1) {
                System.out.println("Im in LandAnimal Class ");
                ((LandAnimal1) (animals[i])).run();

            }
            if (animals[i] instanceof Reptile) {
                System.out.println("Im in Reptile Class ");
                ((Reptile) (animals[i])).crawl();

            }
            if (animals[i] instanceof Poultry) {
                System.out.println("Im in Poultry Class ");
                ((Poultry) (animals[i])).fly();

            }
            if (animals[i] instanceof Aquatic) {
                System.out.println("Im in Aquatic Class ");
                ((Aquatic) (animals[i])).swim();

            }
        }

        System.out.println("Polymorphism Test 2: ");
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i].getName() + " Instanceof Animal " + (animals[i] instanceof Animal1));
            System.out.println(animals[i].getName() + " Instanceof Aquatic " + (animals[i] instanceof Aquatic));
            System.out.println(animals[i].getName() + " Instanceof LandAnimal " + (animals[i] instanceof LandAnimal1));
            System.out.println(animals[i].getName() + " Instanceof Poultry " + (animals[i] instanceof Poultry));
            System.out.println(animals[i].getName() + " Instanceof Reptile " + (animals[i] instanceof Reptile));
            System.out.println(animals[i].getName() + " Instanceof Object " + (animals[i] instanceof Object));
        }

    }
}
