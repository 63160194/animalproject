/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.animal1;

/**
 *
 * @author OS
 */
public class Dog extends LandAnimal1 {

    private String nickname;

    public Dog(String nickname) {
        super("Dog", 4);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Dog name: " + nickname + " run ");
    }

    @Override
    public void eat() {
        System.out.println("Dog name: " + nickname + " eat ");
    }

    @Override
    public void walk() {
        System.out.println("Dog name: " + nickname + " walk ");
    }

    @Override
    public void speak() {
        System.out.println("Dog name: " + nickname + " can't speak ");
    }

    @Override
    public void sleep() {
        System.out.println("Dog name: " + nickname + " sleep ");
    }

}
