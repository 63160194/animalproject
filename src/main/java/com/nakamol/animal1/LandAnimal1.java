/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.animal1;

/**
 *
 * @author OS
 */
public abstract class LandAnimal1 extends Animal1{

    public LandAnimal1(String name, int numberOfLeg) {
        super(name, numberOfLeg);
    }
    
    public abstract void run();
}
