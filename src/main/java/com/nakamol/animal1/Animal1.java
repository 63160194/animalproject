/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.animal1;

/**
 *
 * @author OS
 */
public abstract class Animal1 {

    protected String name;
    protected int numberOfLeg;

    public Animal1(String name, int numberOfLeg) {
        this.name = name;
        this.numberOfLeg = numberOfLeg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfLeg() {
        return numberOfLeg;
    }

    public void setNumberOfLeg(int numberOfLeg) {
        this.numberOfLeg = numberOfLeg;
    }

    public String toString() {
        return " Animal Class: " + name + " Number of Leg: " + numberOfLeg;
    }

    public abstract void eat();

    public abstract void walk();

    public abstract void speak();

    public abstract void sleep();

}
