/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.animal1;

/**
 *
 * @author OS
 */
public class Fish extends Aquatic{
    private String nickname;

    public Fish(String nickname) {
        super("Fish", 0);
        this.nickname = nickname;
    }

    @Override
    public void swim() {
        System.out.println("Fish name: " + nickname + " swim ");
    }

    @Override
    public void eat() {
        System.out.println("Fish name: " + nickname + " eat ");
    }

    @Override
    public void walk() {
        System.out.println("Fish name: " + nickname + " can't walk ");
    }

    @Override
    public void speak() {
        System.out.println("Fish name: " + nickname + " can't speak ");
    }

    @Override
    public void sleep() {
        System.out.println("Fish name: " + nickname + " sleep ");
    }
    
}
